import { Component } from '@angular/core';
import { NavController,LoadingController } from 'ionic-angular';
import { RealspeedPage } from '../realspeed/realspeed';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,public loadingCtrl: LoadingController) {

  }
 pushPage(){
     let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
    this.navCtrl.setRoot('RealspeedPage');
	          loader.dismiss();
  }
}
