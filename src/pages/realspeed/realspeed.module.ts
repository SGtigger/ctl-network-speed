import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RealspeedPage } from './realspeed';

@NgModule({
  declarations: [
    RealspeedPage,
  ],
  imports: [
    IonicPageModule.forChild(RealspeedPage),
  ],
  exports: [
    RealspeedPage
  ]
})
export class RealspeedPageModule {}
